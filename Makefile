# File:   Makefile
# Author: sifRAWR
# Date:   27 Oct 2011
# Descr:  Makefile for scheduler
# Note:   

CC=gcc
CFLAGS=-c -Wall
LDFLAGS=
SOURCES=sssched.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=sssched

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@
	
clean:
	rm -rf *.o *.exe *.out