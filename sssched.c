/** @file   sssched.c
    @author sifRAWR
    @date   27 Oct 2011
*/

#include <stdio.h>

#define CUR_VERSION 0.01

typedef enum {
    TASK_SHUTDOWN,
	TASK_LOGOFF,
	TASK_RESTART,
} task_t;

enum {
	MENU_SET_TIME = 1,
	MENU_SET_TASK,
	MENU_QUIT,
};


static char *tasks_list[] = {
	"Shutdown",
	"Logoff",
	"Restart",
	NULL
};

static task_t task = TASK_SHUTDOWN;
static int task_time[2];

void display_menu(void) {
	printf("Current task: %s\n", tasks_list[task]);
	printf("Current task time: %i:%i\n\n",task_time[0], task_time[1]);
	printf("1. Set schedule time\n"
		   "2. Set scheduled task\n"
		   "3. Quit\n");
}	

int get_input(void) {
	int ret = 0;
	scanf("%i", &ret);
	return ret;
}

void process_input(inp) {
	int hr, min;
	switch (inp) {
	case MENU_SET_TIME:
		printf("1. Set specific time (8:30 am)\n"
			   "2. Set amount of time (2 hours 20 minutes)\n"
			   "3. Back\n");
				int ret = 0;
				scanf("%i", &ret);
				switch (ret) {
				case 1:
					printf("Enter a time in the format 'HOUR MINUTE' (12 30): ");
					scanf("%i %i", &hr, &min);
					task_time[0] = hr;
					task_time[1] = min;
					break;
				default:
					printf("Invalid input.\n");
					break;
				}
		break;
	case MENU_SET_TASK:
		
		break;
	case MENU_QUIT:
		printf("Exiting..\n");
		break;
	default:
		printf("Bad input.\n");
		break;
	}
			
}

int main(void) {
	
	int inp = 0;
	
	printf("Simple Shutdown Scheduler\nVersion %3.2f\n\n", CUR_VERSION);
	
	display_menu();
	inp = get_input();
	while (inp != MENU_QUIT) {
		process_input(inp);
		inp = get_input();
	}
	
	return 0;
}